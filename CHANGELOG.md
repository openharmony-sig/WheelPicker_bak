## v2.1.2-rc.0

- Resolve warn issues


## v2.1.1

- Code obfuscation
- Chore:Added supported device types


## v2.1.1-rc.0

- fix: The month and day columns do not change together in the date selection

## v2.1.0

- 适配V2装饰器

## v2.0.0

- 适配DevEco Studio 版本：4.1 Canary(4.1.3.317)，OpenHarmony SDK:API11 (4.1.0.36)
- ArkTS语法适配

## v1.0.4

- 适配DevEco Studio 3.1Beta1版本。
- 适配OpenHarmony SDK API version 9版本。

## v1.0.3

- api8升级到api9

## v1.0.0

- 已实现功能

1. 循环显示数据项
2. 设置可见数据项条数
3. 在滚轮静止状态直接获取选中数据项
4. 滚动监听获取滚轮停止后选中项以及滚动各项参数
5. 设置当前选中项文本颜色和非选中项文本颜色
6. 设置数据项之间间隔
7. 支持绘制指示器以及指定指示器颜色、尺寸
8. 支持绘制幕布以及指定幕布颜色
9. 可开启数据项空气感模拟
10. 在开启透视或弯曲效果后支持让数据项左右对齐

- 未实现功能

1. 滚轮卷曲效果
   暂不支持3D投影2D，无法实现效果。
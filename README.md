# WheelPicker

## Introduction
> WheelPicker can be used to implement scroll selection with multiple effects based on settings and allows changes to the user interface (UI) effects through the attribute settings. For example, the time picker and area picker can be linked to implement the required effect.

> <img src="./readmeScreen/screen_EN.gif"/>

## How to Install
1. [Install an OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).
2. Run the installation command:
````
ohpm install @ohos/wheelpicker
````

## How to Use

The usage of different pickers is similar. The following uses the time picker as an example.

1. Initialization: Instantiate a **ShowTimePickerComponent.Model** object.

 ```
 @State showWheelPickerLeft: WheelPicker.Model= new WheelPicker.Model()
 ```

2. Attribute setting: Set UI attributes through the **Model** class object to define the required style. You can also add required callbacks.

 ```
private aboutToAppear() {
    this.showWheelPickerLeft
      .setItemAlign(HorizontalAlign.End)
      .setData(this.array)
      .setSelectDataShow((text: string) => {
        prompt.showToast({
          message: "Left:" + text
        });
      })
      }
 ```

3. UI drawing: Invoke the object constructor to pass the instantiated object.

 ```
 
  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Start }) {
      Button("go to:" + this.randomSelect)
        .height(50)
        .padding(10)
        .onClick(() => {
          this.showWheelPickerMiddle.setSelectedItemPosition(this.randomNumber)
          this.randomNumber = Math.floor(Math.random() * (this.array.length + 1))
          this.randomSelect = this.array[this.randomNumber]
        })
      Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Start }) {
        Row() {
          WheelDatePicker()
        }
      }.width('100%').height('100%')
    }
  }
  
 ```

For details, see the implementation on the sample page of the open-source library.

## Available APIs
`showWheelPicker: WheelPicker.Model= new WheelPicker.Model()`

1. Sets the atmosphere.
   `model.setAtmospheric()`
2. Sets the alignment mode.
   `model.setItemAlign()`
3. Sets data of a scrollable list.
   `model.setData()`
4. Specifies whether to set the curtain.
   `model.setCurtain()`
5. Sets the curtain color.
   `model.setCurtainColor()`
6. Specifies whether to set a loop.
   `model.setCyclic()`
7. Sets the font.
   `model.setFontFamily()`
8. Specifies whether to set the indicator.
   `model.setIndicator()`
9. Sets the indicator color.
   `model.setIndicatorColor()`
10. Sets the indicator size.
     `model.setIndicatorSize()`
11. Sets the line spacing of a list.
      `model.setItemSpace()`
12. Sets the data color.
      `model.setItemTextColor()`
13. Sets the data size.
      `model.setItemTextSize()`
14. Sets the color of the selected data.
      `model.setSelectedItemTextColor()`
15. Sets the data height.
      `model.setTextHeight()`
16. Sets the position of a selected item.
      `model.setSelectedItemPosition()`
17. Sets the number of visible items.
      `model.setVisibleItemCount()`
18. Obtains the data height.
      `model.getTextHeight()`
19. Obtains the color of the selected data.
      `model.getSelectedItemTextColor()`
20. Obtains the data size.
      `model.getItemTextSize()`
21. Obtains the data color.
      `model.getItemTextColor()`
22. Obtains the line spacing.
      `model.getItemSpace()`
23. Obtains the scrollbar height.
      `model.getItemHeight()`
24. Obtains the data alignment mode.
      `model.getItemAlign()`
25. Obtains the indicator size.
      `model.getIndicatorSize()`
26. Obtains the indicator color.
      `model.getIndicatorColor()`
27. Obtains the font settings.
      `model.getFontFamily()`
28. Obtains the current item position.
      `model.getCurrentItemPosition()`
29. Obtains the current item data.
      `model.getCurrentItemData()`
30. Obtains the curtain color.
      `model.getCurtainColor()`
31. Obtains the number of visible items.
      `model.getVisibleItemCount()`

## Constraints

This project has been verified in the following versions:

- DevEco Studio NEXT Developer Beta3: 5.0.3.530

- OpenHarmony SDK: API 12 (5.0.0.35)

## Directory Structure
````
|---- WheelPicker  
|     |---- entry  # Sample code
|     |---- library  # WheelPicker library
|	    |----src
          |----main
              |----ets
                  |----components
                      |----wheelpicker.ets # Core implementation of scroll selection
|           |---- index.ets  # External APIs
|     |---- README.md  # Readme      
|     |---- README_zh.md  # Readme                  
````

## About obfuscation
- Code obfuscation, please see[Code Obfuscation](https://docs.openharmony.cn/pages/v5.0/zh-cn/application-dev/arkts-utils/source-obfuscation.md)
- If you want the wheelpicker library not to be obfuscated during code obfuscation, you need to add corresponding exclusion rules in the obfuscation rule configuration file obfuscation-rules.txt：
```
-keep
./oh_modules/@ohos/wheelpicker
```

## How to Contribute
If you find any problem when using the project, submit an [issue](https://gitee.com/openharmony-sig/WheelPicker/issues) or a [PR](https://gitee.com/openharmony-sig/WheelPicker/pulls).

## License
This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-sig/WheelPicker/blob/master/LICENSE).

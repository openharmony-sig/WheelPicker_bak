# WheelPicker

## 简介
> WheelPicker可以实现滚轮选择，通过设置可以实现多种效果，也可以设置属性，改变UI效果，如时间选择器，地区选择器的三级联动，实现需求效果。

> <img src="./readmeScreen/screen.gif"/>

## 下载安装
1. 参考安装教程 [如何安装OpenHarmony ohpm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)
2. 安装命令如下：
````
ohpm install @ohos/wheelpicker
````

## 使用说明

提供多种类型选择器，使用方法类似，以显示时间选择器为例

1、初始化：实例化ShowTimePickerComponent.Model 对象

 ```
 @State showWheelPickerLeft: WheelPicker.Model= new WheelPicker.Model()
 ```

2、属性设置：通过Model类对象设置UI属性来自定义所需风格，也可以添加所需的回调

 ```
private aboutToAppear() {
    this.showWheelPickerLeft
      .setItemAlign(HorizontalAlign.End)
      .setData(this.array)
      .setSelectDataShow((text: string) => {
        prompt.showToast({
          message: "Left:" + text
        });
      })
      }
 ```

3、界面绘制: 调用对象的构造方法，传递已经实例化的对象。

 ```
 
  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Start }) {
      Button("go to:" + this.randomSelect)
        .height(50)
        .padding(10)
        .onClick(() => {
          this.showWheelPickerMiddle.setSelectedItemPosition(this.randomNumber)
          this.randomNumber = Math.floor(Math.random() * (this.array.length + 1))
          this.randomSelect = this.array[this.randomNumber]
        })
      Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Start }) {
        Row() {
          WheelDatePicker()
        }
      }.width('100%').height('100%')
    }
  }
  
 ```

更多详细用法请参考开源库sample页面的实现

## 接口说明
`showWheelPicker: WheelPicker.Model= new WheelPicker.Model()`

1. 设置空气感
   `model.setAtmospheric()`
2. 设置对齐方式
   `model.setItemAlign()`
3. 设置滚轮列表数据
   `model.setData()`
4. 是否设置幕布
   `model.setCurtain()`
5. 设置幕布颜色
   `model.setCurtainColor()`
6. 是否设置循环
   `model.setCyclic()`
7. 设置字体
   `model.setFontFamily()`
8. 是否设置指示器
   `model.setIndicator()`
9. 设置指示器颜色
   `model.setIndicatorColor()`
11. 设置指示器大小
      `model.setIndicatorSize()`
12. 设置列表数据的间隙
      `model.setItemSpace()`
13. 设置数据的颜色
      `model.setItemTextColor()`
14. 设置数据的大小
      `model.setItemTextSize()`
15. 设置选中的数据颜色
      `model.setSelectedItemTextColor()`
16. 设置数据的高度
      `model.setTextHeight()`
17. 设置选中的位置
      `model.setSelectedItemPosition()`
18. 设置可见的个数
      `model.setVisibleItemCount()`
19. 获得数据的高度
      `model.getTextHeight()`
20. 获得选中的数据颜色
      `model.getSelectedItemTextColor()`
21. 获得数据的大小
      `model.getItemTextSize()`
22. 获得数据的颜色
      `model.getItemTextColor()`
23. 获得行间隙
      `model.getItemSpace()`
24. 获得滚轮行的高度
      `model.getItemHeight()`
25. 获得数据的对齐方式
      `model.getItemAlign()`
26. 获得指示器大小
      `model.getIndicatorSize()`
27. 获得指示器颜色
      `model.getIndicatorColor()`
28. 获得字体设置
      `model.getFontFamily()`
29. 获得当前的位置
      `model.getCurrentItemPosition()`
30. 获得当前的数据
      `model.getCurrentItemData()`
31. 获得幕布颜色
      `model.getCurtainColor()`
32. 获得可见的个数
      `model.getVisibleItemCount()`

## 约束与限制

在下述版本验证通过：

- DevEco Studio 版本: NEXT Developer Beta3-5.0.3.530

- OpenHarmony SDK:API12 (5.0.0.35)

## 目录结构
````
|---- WheelPicker  
|     |---- entry  # 示例代码文件夹
|     |---- library  # WheelPicker库文件夹
|	    |----src
          |----main
              |----ets
                  |----components
                      |----wheelpicker.ets #滚轮选择的核心实现
|           |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法      
|     |---- README_zh.md  # 安装使用方法                
````

## 关于混淆
- 代码混淆，请查看[代码混淆简介](https://docs.openharmony.cn/pages/v5.0/zh-cn/application-dev/arkts-utils/source-obfuscation.md)
- 如果希望wheelpicker库在代码混淆过程中不会被混淆，需要在混淆规则配置文件obfuscation-rules.txt中添加相应的排除规则：
```
-keep
./oh_modules/@ohos/wheelpicker
```

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/WheelPicker/issues)给组件，当然，也非常欢迎发 [PR](https://gitee.com/openharmony-sig/WheelPicker/pulls)共建 。

## 开源协议
本项目基于[Apache License 2.0](https://gitee.com/openharmony-sig/WheelPicker/blob/master/LICENSE)，请自由地享受和参与开源。